package idatt2001.oblig3;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Main extends Application {

    Stage window;
    ArrayList<PlayingCard> hand;

    @Override
    public void start(Stage primaryStage) {

        DeckOfCards deck = new DeckOfCards();
        window = primaryStage;
        window.setTitle("Card Game");

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(5,5,5,5));
        grid.setVgap(5);
        grid.setHgap(5);

        Label handAmountLable = new Label("Amount of cards on hand:");
        GridPane.setConstraints(handAmountLable,0,0);
        TextField handAmountInput = new TextField("5");
        GridPane.setConstraints(handAmountInput,1,0);
        Button buttonDealHand = new Button("Deal hand");
        GridPane.setConstraints(buttonDealHand,2,0);

        Label handDisplay = new Label("");
        handDisplay.setWrapText(true);
        handDisplay.setMaxWidth(500);
        handDisplay.setFont(new Font("Times New Roman", 20));
        GridPane.setConstraints(handDisplay,0,2);

        buttonDealHand.setOnAction(e -> {
            int handAmount = Integer.parseInt(handAmountInput.getText());
            if (handAmount<5){
                handDisplay.setText("Error! Can not deal " + handAmount + " cards. Hand has to be 5 cards minimum.");
            } else if (deck.getDeck().size()<handAmount){
                handDisplay.setText("Error! Less than " + handAmount + " cards left in deck.");
            } else {
                this.hand = deck.dealHand(handAmount);
                String handString = "";
                for (PlayingCard c : hand) {
                    handString += c.getAsString() + "   ";
                }
                handDisplay.setText(handString);
            }
        });

        Button buttonCheckHand = new Button("Check hand");
        GridPane.setConstraints(buttonCheckHand,0,4);

        Label checkHandDisplay = new Label("");
        checkHandDisplay.setWrapText(true);
        checkHandDisplay.setMaxWidth(500);
        GridPane.setConstraints(checkHandDisplay,0,6);

        buttonCheckHand.setOnAction(e -> {
            CheckHand checkHand = new CheckHand(hand);
            boolean flush = checkHand.checkFlush();
            if(flush){
                checkHandDisplay.setText("The hand is a Flush");
            } else{
                checkHandDisplay.setText("The hand is NOT a Flush");
            }
        });

        //TODO: code checkHand


        grid.getChildren().addAll(handAmountLable,handAmountInput,buttonDealHand,handDisplay,buttonCheckHand,checkHandDisplay);
        Scene scene = new Scene(grid,600,400);
        window.setScene(scene);
        window.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
