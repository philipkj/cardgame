package idatt2001.oblig3;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class used to check contents of a hand
 */
public class CheckHand {

    private ArrayList<PlayingCard> hand;

    /**
     * Constructor
     * @param hand list with cards on hand
     */
    public CheckHand(ArrayList<PlayingCard> hand){
        this.hand = hand;
    }

    /**
     * Method checking if hand is a flush
     * @return boolean
     */
    public boolean checkFlush(){
        char suit = hand.get(0).getSuit();
        List<PlayingCard> flushCheck = hand.stream().filter(p -> p.getSuit()==suit).collect(Collectors.toList());
        return flushCheck.size() == hand.size();
    }


}
