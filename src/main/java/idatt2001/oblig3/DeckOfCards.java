package idatt2001.oblig3;

import java.util.ArrayList;
import java.util.Random;

/**
 * The class DeckOfCards is a representation of a full deck of 52 playing cards
 */
public class DeckOfCards {

    private ArrayList<PlayingCard> deck;
    private final char[] suit = { 'S', 'H', 'D', 'C' }; // Spades, Hearts, Diamonds, Clubs

    /**
     * The constructor creates the full deck, consisting of all the 52 cards
     */
    public DeckOfCards(){
        deck = new ArrayList<>();

        for (char s : suit){
            for (int i = 1; i<14; i++){
                deck.add(new PlayingCard(s,i));
            }
        }
    }

    /**
     * Deals a specified amount of random cards to the hand
     * @param n amount of cards to be dealt
     */
    public ArrayList<PlayingCard> dealHand(int n){
        if(n > deck.size()){
            return null; //Potential error message
        } else{
            ArrayList<PlayingCard> cardsDealtToHand = new ArrayList<>();
            for (int i=0; i<n; i++){
                Random random = new Random();
                int randomNumber = random.nextInt(deck.size());
                cardsDealtToHand.add(deck.get(randomNumber));
                deck.remove(deck.get(randomNumber));
            } return cardsDealtToHand;
        }
    }

    public ArrayList<PlayingCard> getDeck(){
        return deck;
    }


}
