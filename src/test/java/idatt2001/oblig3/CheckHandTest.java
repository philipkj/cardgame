package idatt2001.oblig3;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests for the class CheckHand
 */
public class CheckHandTest {

    /**
     * Checks the method when outcome is supposed to be true
     */
    @Test
    void testFlushTrue(){
        ArrayList<PlayingCard> hand1 = new ArrayList<>();
        hand1.add(new PlayingCard('C', 1));
        hand1.add(new PlayingCard('C', 2));
        hand1.add(new PlayingCard('C', 3));
        CheckHand checkHandTrue = new CheckHand(hand1);
        boolean flushTrue = checkHandTrue.checkFlush();
        assertTrue(true);
    }

    /**
     * Checks the method when outcome is supposed to be false
     */
    @Test
    void testFlushFalse(){
        ArrayList<PlayingCard> hand2 = new ArrayList<>();
        hand2.add(new PlayingCard('S', 11));
        hand2.add(new PlayingCard('H', 4));
        hand2.add(new PlayingCard('H', 6));
        CheckHand checkHandFalse = new CheckHand(hand2);
        boolean flushFalse = checkHandFalse.checkFlush();
        assertFalse(false);
    }
}
