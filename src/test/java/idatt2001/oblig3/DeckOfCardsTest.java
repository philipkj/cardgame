package idatt2001.oblig3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

/**
 * Test for the class DeckOfCards
 */
public class DeckOfCardsTest {

    DeckOfCards testingDeck = new DeckOfCards();

    /**
     * Counts the cards in the deck
     */
    @Test
    void testDeckOfCards(){
        System.out.println("The deck contains " + testingDeck.getDeck().size() + " cards.");
    }

    /**
     * Prints all the cards in the deck
     */
    @Test
    void testGetCards(){
        ArrayList<PlayingCard> deck = testingDeck.getDeck();
        for (int i=0; i<deck.size(); i++){
            System.out.println(deck.get(i).getAsString());
        }
    }

    /**
     * Prints the dealt hand and checks if the hand contains duplicates
     */
    @Test
    void testDealHand(){
        ArrayList<PlayingCard> hand = testingDeck.dealHand(10);
        boolean duplicate = false;
        for (PlayingCard c : hand){
            System.out.println(c.getAsString());
        }
        for (int i = 0; i<hand.size();i++){
            for (int j = 0; j<hand.size(); j++){
                if (i!=j && hand.get(i).getAsString().equals(hand.get(j).getAsString())){
                    duplicate = true;
                }
            }
        } System.out.println(duplicate);
    }
}
